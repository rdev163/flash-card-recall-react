Flashcard Recall React
Semantic-Version [SemVer.org](http://sem-ver.org)
>Please log changes made to source code here. Thank you.

# v0.0.1 - 9/16/2017

[code] - built `user.model`, `decks.model`, `table.component`, `flash-card.component`, and `game-instance-manager` 
[code] - Webpack 3 has been brought in with `webpack.config`
[refactor] - Changed `.ts` extentions all to `.tsx`

# v0.0.2 - 9/18/2017
[code] - built redux reducers and stores, `user.store` and `flashcard.store`  
[refactor] - reworked `game-instance-manager.component` to hold all the game logic and is a single point of truth
[code] - built `splash-screen.component`
[style] - added `main.style.scss` and `panel.style.scss` to the application data flow, need to run webpack to load it because its not parsed by anything. 


# v 0.0.3 - 9/20/2017
[code] - built amd finished `Start-Screen.component`, `flash-card.component` and `summary.component`
[test] - built a few tests for the `Flash-Card.component` and began to implement a full DOM testing render for `GameInstanceManager.component`
[code] - Added to and organized functions in the `GameInstanceManager.Component`

# v 0.0.4 - 9/25/2017
[code, refactor] - `GameInstanceManager.component` is updated and works for starting the game and presenting questions which will return true or false with an answer.
[test] - `App.test` is running two suits of tests for `GameInstanceManager.component` and `FlashCardComponent`
[refactor] - `FlashCard.model` and `Deck.model` answer to answers
[code] - `StartScreenComponent.cnmponent` is using a callback-ish function to trigger game start.

# v 0.0.5 - 9/26/2017
[code, refactor] - `GameInstanceManager.component` has been organized and built upon with a complete flow for questions and answers, winning condition still isnt triggerable and there is abug with the start process and rendering.
[code, refactor] - Time Travel state persistence has been implemented for statistical gathering of user interactions with the game.
[code] - `Panel.component` has been wired up and is being debugged
[test] - 2 suites are functional (though it says one...) and now implementing red to green development.

# v 0.0.6 - 9/27/2017
[bugfix] - `GameInstanceManager` was not incrementing missed and matched answers properly, it's been fixed. A weir behavior with an instance var being assigned a value even before the value is assigned to it is present, though it is not a blocker. https://stackoverflow.com/questions/45904252/javascript-tsx-react-how-is-this-instance-variable-getting-values-assigned-w
   
# v 0.0.7 - 9/28/2017
[WIP - HistoryManagement] - Implementing Timetravel state structures for the ability to provide metrics for cards and weight them, still work in progress.

# v 0.0.8 - 9/28/2017
[refactor] - `GameInstanceManager` was split into `GameManager.component`  and `GameInstance.component`, the dispatcher is now a closure callback function and will trigger a render.
[HistoryManagement] - State chains are now stored in `GameManager.component` and is retrievable.

# v 0.0.9 - 9/29/2017
[code-refactor] - Updated and built new callbacks for the dispatcher and renamed vars and functions to adhear to paradigms, also finished flow from game start to game end with a line in `GameManager` to trigger win.
[HistoryManagment] - History chains are implemented and stored in `GameManager`
 
# v 0.0.10 - 9/30/2017
[code] - Built `ListComponent` which accepts  an object and will output it's keys and values. 
