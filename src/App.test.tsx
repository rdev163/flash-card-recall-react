import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {DECKS} from './models/decks.model';
import FlashCardComponent from './components/flash-card.component';
import * as TestUtils from 'react-dom/test-utils';
import {default as GameManager, HistoryEnhancer, STATE} from './GameManager';
import {GameInstance} from './components/game-instance.component';

const fixtureDiv0 = document.createElement('div');


describe('Default test', () => {
	fixtureDiv0.id = 'TestDiv0';

	document.getElementsByTagName('body')[0].appendChild(fixtureDiv0);
		it('renders without crashing', () => {
			ReactDOM.render(<GameManager/>, fixtureDiv0);
	});
});

describe('Flashcard Render and mode tests', () => {
const fixtureDeck = DECKS('demo');

	it('renders unique text for each mode that matches the card\'s data.', () => {
		const flashCard: any = TestUtils
			.renderIntoDocument(
				<FlashCardComponent mode={'question'} state={{present: { gameInstance: {currentCard: fixtureDeck[3]}}} as HistoryEnhancer<STATE>} dispatcher={() => {}} />
			);
		expect(TestUtils.scryRenderedDOMComponentsWithClass(flashCard as React.Component, 'question-box')[0].firstChild.nodeValue).toBe(fixtureDeck[3].question.toString());
	});

	it('renders a different text node in \'answer\' mode', () => {
		const flashCard: any = ReactDOM.render(
			<GameManager dispatcher={() => {}} state={{} as HistoryEnhancer<STATE>} mode={'answer'} />,
			fixtureDiv0
		);
		flashCard.stateStore.next({present: {viewShow: {answer: true}}, gameInstance: { deck: fixtureDeck, currentCard: fixtureDeck[0]}});
		flashCard.setState();
		console.log('flash', flashCard);
		expect(TestUtils.findRenderedDOMComponentWithClass(flashCard as any, 'answer-box').firstChild.nodeValue).toMatch(fixtureDeck[3].answers[0]);
	});
});

const renderGameManager: any = (dispatcher: () => void, state: HistoryEnhancer<STATE>) => {
	return TestUtils.renderIntoDocument(<GameInstance decks={DECKS} state={state} dispatcher={dispatcher}/>)
};


describe('GameInstanceManager Tests', () => {

	it('default screen', () => {
		const found = TestUtils.scryRenderedDOMComponentsWithTag(renderGameManager({}), 'h1')[0].textContent;
		expect(found)
			.toBe('Flashcard Recall Drill by RDev;)');
	});

	// it('should have a question screen with text that matches the card question used to initialize.', () => {
	// 	const component: any = TestUtils.renderIntoDocument(
			{/*<div className="TOP">*/}
				{/*<FlashCardComponent mode={'answer'} state={{present: { gameInstance: { currentCard: fixtureDeck[3] }}} as HistoryEnhancer<STATE>} dispatcher={() => {}}/>*/}
			// </div>
		// expect(component.getElementsByClassName('question-box')[0].innerHTML).toEqual(fixtureDeck[0].question)};
	//
	// it('should show newline delimited text inside the class answers-box that matches the text array in the answers property of the card.', () => {
	// 	const component: any = TestUtils.renderIntoDocument(
			{/*<div>*/}
				{/*<FlashCardComponent mode={'answer'} state={{present: { gameInstance: { currentCard: fixtureDeck[3] }}} as HistoryEnhancer<STATE>} dispatcher={() => {}}/>*/}
			// </div>
		// expect(renderGameManager({present: {stats: {misses: 4, matched: 7, streak:4, answered: 11}}}).showGiftShop().stats).toMatchObject({streak: 4, miss: 4, matched: 7});
		//
		// expect(renderGameManager({}).getElementsByTagName('button').length === 3).toBeTruthy();
	// });
});

describe('History and Time Travel Managment', () => {
	it('should be able to store a state chain array in the [past] property of this.state ', ()=> {
		const GameManager:any  = TestUtils.renderIntoDocument(
		<div>
					<GameInstance dispatcher={() => {}} decks={DECKS} state={{} as HistoryEnhancer<STATE>}/>
				</div>
			);
		expect(GameManager.props).toEqual(0);
	});

	it('should store a new state link when the dispatcher is called.', () => {
		const gameManager = renderGameManager({});
		expect(()=> gameManager.state.past.length && gameManager.dispatch({type:'INIT_DEMO'})).toBeLessThan(gameManager.state.past.length)
	})
});
