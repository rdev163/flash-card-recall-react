import * as React from 'react';
import './App.css';
import {GameInstance} from './components/game-instance.component';
import {FlashCard} from './models/flashcard.model';
import {DECKS, FlashCardDeck} from './models/decks.model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';

export const History_Init = (init) => {
	return {
		past: [],
		present: init,
		future: []
	}
};

export interface STATE {
	gameInstance: {
		deck: FlashCardDeck;
		currentCard: FlashCard;
		goal: number;
		gg: boolean;
		stats: {
			misses: number;
			matches: number;
			streak: number;
			answered: number;
			// toughestCard: () => any;
			// easiestCard: () => any;
		}
	};
	viewShow: {
		start: boolean,
		menu: boolean,
		question: boolean,
		answers: boolean,
		summary: boolean,
		theGiftShop: boolean
	}
}

export interface Action {
	type: string;
	payload?: any;
}

export interface HistoryEnhancer<S> {
	past: S[];
	present: S;
	future: S[];
}

class GameManager extends React.Component {
	public decks = DECKS;
	public stateReady: boolean = false;
	public cardQueue: FlashCard[] = [];
	public state: HistoryEnhancer<STATE>;
	public STATE_INIT = {
		gameReady: false,
		gameInstance: {
			goal: null,
			gameReady: false,
			deck: this.decks('demo_array'),
			currentCard: {},
			gg: false,
			stats: {
				misses: 0,
				matches: 0,
				streak: 0,
				answered: 0,
			},
		},
		viewShow: {
			start: false,
			menu: false,
			question: false,
			answers: false,
			tryAgain: false,
			theGiftShop: false,
		}
	};
	public stateStore: BehaviorSubject<HistoryEnhancer<STATE>> = new BehaviorSubject<HistoryEnhancer<STATE>>(History_Init(this.STATE_INIT));
	public stateStore$: Observable<HistoryEnhancer<STATE>> = this.stateStore.asObservable();

	constructor(public props){
		super(props);
		this.dispatcher = this.dispatcher.bind(this);
		this.GameManagerReducer = this.GameManagerReducer.bind(this);
		this.historyEnhancer= this.historyEnhancer.bind(this);
		this.state = History_Init(this.STATE_INIT);
	}

	public componentDidMount(): void {
		this.stateStore$.subscribe(update => {
			// Render on update
			this.setState(update);
			this.stateReady = true;

		});
		this.dispatcher({type: 'GAME_STARTED'});
	}

	public dispatcher(action): void {
		const historyGameReducer = this.historyEnhancer(this.GameManagerReducer);
		const currentState = this.stateStore.value;
		const newHistory = historyGameReducer(action, currentState);
		console.log('Dispatcher next state', action.type, currentState, newHistory);
		this.stateStore.next(newHistory);
	};

	public historyEnhancer(reducer: (action, state) => STATE): ((action: Action, state) => HistoryEnhancer<STATE> ) {
		return function (action: Action, state: HistoryEnhancer<STATE>): HistoryEnhancer<STATE> {
			if(!state) {
				return;
			}
			const {past, present, future} = state;

			switch (action.type) {

				case'BACK':
					if (past.length < 1) {
						return state;
					}
					const previous = past[past.length - 1];
					const newPast: STATE[] = past.slice(0, -1);
					return {
						past: newPast,
						present: previous,
						future: [present, ...future]
					};

				case'FORWARD':
					if (future.length < 1) {
						return state;
					}
					const next = future[0];
					const newFuture = future.slice(1);
					return {
						past: [...past, present],
						present: next,
						future: newFuture
					};

				default:
					const newPresent: STATE = reducer(action, present);
					// if (present === newPresent) {
					// 	return state;
					// }
					return  {...state, past: [...past, present], present: newPresent, future: [] }
			}
		}
	}

	public GameManagerReducer(action: Action, state: STATE){
		if(!action){

			return state;
		}
		const{ type, payload} = action;
		console.log('payload', payload);
		switch(type) {
			case 'END_GAME_SELECTED':
				window.close();
			case 'GAME_STARTED':
				return {
					...state,
					gg: false,
					gameInstance: {
						...state.gameInstance,
						currentCard: {} as FlashCard,
						stats: {
							...state.gameInstance.stats,
							misses: 0,
							matches: 0,
							streak: 0,
							answered: 0,
						},
					},
					viewShow: {
						...state.viewShow,
						question: false,
						answers: false,
						start: true,
						theGiftShop: false
					}
				};

			case 'CARD_REQUESTED':
				return {
					...state,
					gameInstance: {
						...state.gameInstance,
						stats: {
							...state.gameInstance.stats
						}
					}, viewShow: {
						...state.viewShow,
						start: false,
						answers: false,
						tryAgain: false,
						question: true
					}
				};

			case 'GAME_WON':
				return{
					...state,
					gameInstance: {
						...state.gameInstance,
						stats: {
							...state.gameInstance.stats
						}
					},
					viewShow: {
						...state.viewShow,
						start: false,
						theGiftShop: true,
						answers: false,
						question: false,
					}
				};

			case 'QUESTION_ANSWERED':
				const stats = this.verifyAnswer(payload.answer, state.gameInstance.currentCard.answers)

				// const stats = (true === true) // For testing
					?
						{
							answered: state.gameInstance.stats.answered + 1,
							streak: state.gameInstance.stats.streak + 1,
							matches: state.gameInstance.stats.matches + 1
						}
					:
						{
							answered: state.gameInstance.stats.answered + 1,
							streak: 0,
							misses: state.gameInstance.stats.misses + 1
						};

				// check for win
				if(stats.streak === state.gameInstance.goal) {
					return {
						...state,

						gameInstance: {
							...state.gameInstance,
							gg: true,
							stats: {
								...state.gameInstance.stats,
								...stats,
							}
						},
						viewShow: {
							...state.viewShow,
							question: false,
							answers: true,
						}
					}
				}

				return {
					...state,
					gameInstance: {
						...state.gameInstance,
						stats: {
							...state.gameInstance.stats,
							...stats
						}
					},
					viewShow: {
						...state.viewShow,
						question: false,
						answers: true
					}
				};

			case 'DRILL_STARTED' :
				console.log('GOALS!!!',  + payload.deck.cards.length);
				this.cardQueue = payload.deck.cards;
				const card = this.drawCard(state);
				return {
					...state,
					gameInstance: {
						...state.gameInstance,
						deck: payload.deck,
						goal: payload.deck.cards.length + 1,
						currentCard: card,
						stats: {
							...state.gameInstance.stats,
							misses: 0,
							matches: 0,
							streak: 0,
							answered: 0,
						},
					},

					viewShow: {
						...state.viewShow,
						start: false,
						menu: false,
						question: true,
						answers: false,
						tryAgain: false,
						theGiftShop: false,
					}
				};


			case 'TURN_STARTED':
				const currentCard = this.drawCard(state);
				return {
					...state,
					gameInstance: {
						...state.gameInstance,
						currentCard: currentCard,
						stats: {
							...state.gameInstance.stats
						}
					},
					viewShow: {
						...state.viewShow,
						question: true,
						start: false,
						answers: false}
				}

		}

	}

	private drawCard(state: STATE): FlashCard {
		// const card = this.cardQueue.slice(-1, -1);
		const card = this.cardQueue.splice(0, 1)[0];
		if(Object.keys(state.gameInstance.currentCard).length > 0 ){
			this.cardQueue = this.cardQueue.concat([state.gameInstance.currentCard]);
			console.log('this queue 2', state.gameInstance.currentCard, this.cardQueue);
		}
		return card;
	}

	private verifyAnswer(answer: string, matchers: string[]): boolean {
		console.log('ANSWER', answer);
		let strippedMatchers: string[] = [];
		matchers.forEach((val) => {
			strippedMatchers = [
				...strippedMatchers,
				val.toLowerCase()
					.trim()
					.replace(',' || '.' || '"','' )
			]
		});

		const strippedAnswer = answer
			.toLowerCase()
			.trim()
			.replace(',' || '.' || '"','' );
		return _.includes(strippedMatchers, strippedAnswer);
	}

	render() {
		if(!this.stateReady) {
			return (
				<h2>Data Loading . . . </h2>
			)
		}
		return (
			<GameInstance decks={DECKS} dispatcher={this.dispatcher} state={this.stateStore.value} />
		);
	}
}

export default GameManager;
