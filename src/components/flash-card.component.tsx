import * as React from 'react';
import {Action, HistoryEnhancer, STATE} from '../GameManager';
export interface FlashCardProps {
	state: HistoryEnhancer<STATE>;
	mode: string;
	dispatcher: (action: Action) => void;
}

export class FlashCardComponent extends React.Component {
	public questionResponse: string = '';

	constructor(
		public props: FlashCardProps
	){
		super(props);
	}

	public answers(): string {
		let answerString = '';
		for(let answer of this.props.state.present.gameInstance.currentCard.answers) {
			answerString += answer + '\n';
		}
		return answerString;
	}

	public componentWillUnmount(): void {
		console.log('FLASHCARD SUICIDE');
	}

	public onSubmit(): void {
		const response = this.questionResponse;
		this.props.dispatcher({type: 'QUESTION_ANSWERED', payload: {questionResponse: response}});
	}

	private onChangeHandler(event: any): void {
		this.questionResponse = event.target.value;
	}

	private onNextHandler(){
		this.props.state.present.gameInstance.gg
			?
		this.props.dispatcher({type: 'GAME_WON'})
			:
		this.props.dispatcher({type: 'TURN_STARTED'});
	}

	private renderCardAsMode(mode: string): any {
		switch(mode){
			case'question':
				return(
					<div className="flashcard">
						Question:<strong className="question-box">{this.props.state.present.gameInstance.currentCard.question}</strong>
						<hr/>
						<input type="text" id="InputAnswer" onChange={(event) => this.onChangeHandler(event)}/>
						<button className="button" id="ButtonSubmitAnswer" onClick={() => {this.onSubmit()}}>Submit</button>
					</div>
				);
			case'answer':
				return(
					<div className="flashcard">
						<strong className="answer-box" >Answer:{this.answers()}</strong>
						<button id="ButtonNextCard" onClick={() => this.onNextHandler()}>Next Card</button>
					</div>
				);
			default:
				console.log('no card mode found');
				return;
		}
	}

	public render(){
		return (
			<div className="flashcard-box">
				{this.renderCardAsMode(this.props.mode)}
			</div>
		)
	}
}

export default FlashCardComponent
