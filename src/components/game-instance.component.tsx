import * as React from 'react';
import {FlashCardDeck} from '../models/decks.model';
import FlashCardComponent from './flash-card.component';
import {PannelComponent} from './pannel.component';
import {HeaderComponent} from './header.component';
import {StartScreenComponent} from './start-screen.component';
import {SummaryComponent} from './summary.component';
import {STATE, HistoryEnhancer, Action} from '../GameManager';

// This is the smartest component in regards to the Flashcard Game,
// here would be business logic and state that effect the users progression through the drills
export interface InstanceProps {
	dispatcher: (action: Action) => void;
	state: HistoryEnhancer<STATE>;
	decks: any;
}

export class GameInstance extends React.Component {

	constructor(public props: InstanceProps
	){
		super(props);
	}

	public renderElement = {

		renderSummary: (state: HistoryEnhancer<STATE>, dispatcher: ((action: Action) => void)) => {
			return <SummaryComponent state={state} dispatcher={dispatcher} />
		},

		renderHeader: (title: string) => {
			console.log('header title', title);
			return <HeaderComponent title={title}></HeaderComponent>
		},

		renderPanel: (stats) => {
			return <PannelComponent {...stats} />
		},

		renderStartMenu: ( decks: () => FlashCardDeck, dispatcher: (action: Action) => void) => {
			return <StartScreenComponent decks = {decks} state={this.props.state} dispatcher={dispatcher}/>
		},

		renderCard: (mode: string, state: HistoryEnhancer<STATE>, dispatcher: (action: Action) => void) => {
			return (
				<FlashCardComponent mode={mode} state={state} dispatcher={dispatcher} />
			)
		}
	};

	render(){
		console.log('the props', this.props);
		const deckTitle = this.props.state.present.gameInstance.deck ? this.props.state.present.gameInstance.deck.meta.title : 'No Deck Selected';
		return (
			<div className="gameInstManager">
				<h1>Flashcard Recall Drill <small>by RDev;)</small></h1>
				{this.renderElement.renderHeader(this.props.state.present.gameInstance.deck.meta.title)}

				{/*start-menu*/}
				{this.props.state.present.viewShow.start === true && this.renderElement.renderStartMenu(this.props.decks, this.props.dispatcher)}

				{/*question view*/}
				{this.props.state.present.viewShow.question === true && this.renderElement.renderCard('question', this.props.state, this.props.dispatcher )}

				{/*//answer or tryAgain view*/}
				{this.props.state.present.viewShow.answers === true && this.renderElement.renderCard('answer', this.props.state, this.props.dispatcher )}

				{/*//summery view*/}
				{this.props.state.present.viewShow.theGiftShop === true && this.renderElement.renderSummary(this.props.state, this.props.dispatcher)}
				{this.renderElement.renderPanel(this.props.state.present.gameInstance.stats)}
			</div>
		)
	}

	//breakdown process after game is over

}

export default GameInstance;