import * as React from 'react'

export interface HeaderProps {
	title: string;
}
export class HeaderComponent extends React.Component {
	constructor(public props: HeaderProps){
		super(props);
		this.props = props;
	}

	componentDidMount(){
		console.log('HEADER PROPS', this.props);
	}

	render(){
		return(
			<div className="header">
				Current Deck Selected:
				<h1>{this.props.title}</h1>
			</div>
		)
	}
}