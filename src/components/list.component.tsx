import * as React from 'react';

export interface ListComponentProps {
	items: {}
}
export class ListComponent extends React.Component{

	constructor(public props: ListComponentProps){
		super(props);
		this.state = ({})
	}

	render(){
		return(
			<ul>
				{Object.keys(this.props.items).map(val => {
					return <li key={val.toString()}><strong>{val}:</strong> {this.props.items[val]}</li>
				})}
			</ul>
		);
	}
}

