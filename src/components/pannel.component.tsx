import * as React from 'react'

export interface PanelProps {
	matches: number;
	misses: number;
	streak: number;
	answered: number;
}

export class PannelComponent extends React.Component {
	props: PanelProps;

	constructor(props){
		super(props);
		console.log('PANEL PROPS', this.props)
	}

	render(){
		return(
			<div className="panel">
				<div className="matches">
					Right: {this.props.matches}
				</div>
				<div>
					Answered: {this.props.answered}
				</div>
				<div>
					Missed: {this.props.misses}
				</div>
				<div>
					Current Streak: {this.props.streak}
				</div>
			</div>
			)
	}
}