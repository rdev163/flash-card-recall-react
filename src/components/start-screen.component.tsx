
import * as React from 'react';
import {Action, HistoryEnhancer, STATE} from '../GameManager';
import {FlashCardDeck} from '../models/decks.model';
import {ListComponent} from './list.component';

export interface StartScreenProps {
		decks: (key: string) => FlashCardDeck;
		state: HistoryEnhancer<STATE>;
		dispatcher: (action: Action) => void;
}

export class StartScreenComponent extends React.Component {
	//deck selection comming soon ...
	constructor(public props: StartScreenProps){
		super(props);
	}

	public handleOnClick(): void {
		const deck = this.props.decks('demo_array');
		this.props.dispatcher({type: 'DRILL_STARTED', payload: {foo: 'bar', deck:deck}});
	}

	render(){

		return (
			<div className="start-menu">
				<img src="../assets/imgs/flashcard-recall-banner-2500.jpeg" className="banner-img" />
				<h1>Flashcard React for Recall</h1>
				<button className="btn btn-lg" id="ButtonStartGame" onClick={() => {this.handleOnClick()}}>Start Game</button>
			</div>
		)
	}
}