import * as React from 'react';
import {Action, HistoryEnhancer, STATE} from '../GameManager';
import {ListComponent} from './list.component';

export interface SummeryProps {
	dispatcher: (action: Action) => void;
	state: HistoryEnhancer<STATE>;
}

export class SummaryComponent extends React.Component {

	constructor(public props: SummeryProps){
		super(props)
	}

	public render(): React.ReactElement<SummaryComponent> {

			return(
				<div className="summaryComponent">
					<h1> Well Done! You have mastered {this.props.state.present.gameInstance.deck.meta.title} deck of {this.props.state.present.gameInstance.deck.meta.totalCards}.</h1>
					<div className="stats-list">
						<ListComponent items={this.props.state.present.gameInstance.stats} />
					</div>
					<div className="summaryOptions">
						<button id="ButtonTryAgain" onClick={() => this.props.dispatcher({type: 'GAME_STARTED'})}>Again!</button>
						{/*<button id="ButtonAnotherDeck" onClick={() => this.props.dispatcher({type: 'PICK_NEW_DECK_SELECTED'})}>Pick Another Deck</button>*/}
						{/*<button id="ButtonDone" onClick={() => this.props.dispatcher({type: 'END_GAME_SELECTED'})}>Done</button>*/}
					</div>
				</div>
		)
	}
}