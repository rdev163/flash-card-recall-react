import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {GameInstance} from './components/game-instance.component';
import './styles/main.style.scss';
import GameController from './GameManager';

ReactDOM.render(
 <GameController />,
  document.getElementById('root')
);

registerServiceWorker();
