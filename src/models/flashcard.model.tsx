import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {FlashCard} from './decks.model';

export interface FlashCardProps {
	card: FlashCard;
	mode: string;
	callback?: (e: any) => void;
	fbs?: BehaviorSubject<any>;
	[name:string]: any;
}

export interface FlashCardComponentState {
	answer: string;
	mode: string;
}

export interface FlashCard {
	cardId: string;
	deckId?: string;
	mode?: string;
	question?: string;
	answers?: string[];
	onSubmit: () => any;
	title: string;
	points?: number;
}
