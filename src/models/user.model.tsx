import {FlashCard} from './flashcard.model';

export interface UserModel {
	id: string,
	userName: string;
	email: string;
	mobile: string;
	stats: {
		[name:string]: any;
	}
}

export interface UserStats {
	misses: number;
	matches: number;
	streak: number;
	answered: number;
	toughestCard: FlashCard;
	easiestCard: FlashCard;
}
